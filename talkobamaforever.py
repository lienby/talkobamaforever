import os
import requests
import sys
import moviepy.editor


def download(link, file_name):
    with open(file_name, "wb") as f:
            print "Downloading video %s" % file_name
            response = requests.get(link, stream=True)
            total_length = response.headers.get('content-length')

            if total_length is None: # no content length header
                f.write(response.content)
            else:
                dl = 0
                total_length = int(total_length)
                for data in response.iter_content(chunk_size=4096):
                    dl += len(data)
                    f.write(data)
                    done = int(50 * dl / total_length)
                    sys.stdout.write("\r[%s%s]" % ('=' * done, ' ' * (50-done)) )
                    sys.stdout.flush()
            print ""

def create_video(text):


    print "Processing..."
    page = requests.post(url="http://talkobamato.me/synthesize.py",data={"input_text":text})
    url = page.url
    if page.text.__contains__('<source src="'):
        print "Processing Done!"
        start = page.text.index('<source src="') + len('<source src="')
        end = page.text.index('" type="video/mp4">')
        return "http://talkobamato.me/" + page.text[start:end]
    while True:
        r = requests.get(url)
        if r.text.__contains__('<source src="'):
            print "Processing Done!"
            start = r.text.index('<source src="')+len('<source src="')
            end = r.text.index('" type="video/mp4">')
            return "http://talkobamato.me/"+r.text[start:end]

def create_longer_video(text):
    print "String longer than 180 characters, will send in 180 character long segments."
    count = 1
    while True:
        current = text[:180]
        print "Sending Segment: "+str(count)
        download(create_video(current),"obama"+str(count)+".mp4")
        text = text[180:]
        count += 1
        if not len(text) > 180:
            print "Sending Segment: {}".format(count)
            download(create_video(text),"obama"+str(count)+".mp4")
            break
    print "Joining Videos Together..."
    count = 1
    clips = []
    while True:
        if os.path.exists("obama"+str(count)+".mp4"):
            clips.append(moviepy.editor.VideoFileClip("obama"+str(count)+".mp4"))
            print "Adding: obama"+str(count)+".mp4"
        else:
            break
        count += 1
    render = moviepy.editor.concatenate_videoclips(clips)
    if os.path.exists("obama.mp4"):
        os.remove("obama.mp4")
    render.write_videofile("obama.mp4")
    count = 1
    while True:
        if os.path.exists("obama"+str(count)+".mp4"):
            os.remove("obama"+str(count)+".mp4")
        else:
            break
        count += 1
    print "DONE Output File - Obama.mp4"
if os.path.exists("obama.txt"):
    inp = open("obama.txt","r").read()
else:
    inp = raw_input("Enter String: ")


inp = inp.replace("1", " one ")
inp = inp.replace("2", " two ")
inp = inp.replace("3", " three ")
inp = inp.replace("4", " four ")
inp = inp.replace("5", " five ")
inp = inp.replace("6", " six ")
inp = inp.replace("7", " seven ")
inp = inp.replace("8", " eight ")
inp = inp.replace("9", " nine ")
inp = inp.replace("0", " zero ")
if len(inp) > 180:
    create_longer_video(inp)
else:
    download(create_video(inp),"obama.mp4")
